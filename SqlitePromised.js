"use strict"

var sqlite3 = require('sqlite3');

class SqlitePromised
{
    constructor()
    {
        this.db = null;
    }
    
    open(filename, mode)
    {
        if (!mode)
            mode = sqlite3.OPEN_CREATE | sqlite3.OPEN_READWRITE

        return new Promise((resolve, reject) => {
            this.db = new sqlite3.Database(filename, mode , function(err) {
                if (err)
                    reject(err);
                else
                    resolve();
            });
        });
    }

    close()
    {
        return new Promise((resolve, reject) => {
            this.db.close((err) => {
                if (err)
                    reject(err);
                else
                    resolve();
            });
        });
    }

    run(sql, params)
    {
        return new Promise((resolve, reject) => {
            this.db.run(sql, params, function(err, result) {
                if (err)
                    reject(err);
                else
                    resolve({
                        lastID: this.lastID,
                        changes: this.changes
                    });
            });
        });
    }

    get(sql, params)
    {
        return new Promise((resolve, reject) => {
            this.db.get(sql, params, (err, result) => {
                if (err)
                    reject(err);
                else
                    resolve(result);
            });
        });
    }

    all(sql, params)
    {
        return new Promise((resolve, reject) => {
            this.db.all(sql, params, (err, result) => {
                if (err)
                    reject(err);
                else
                    resolve(result);
            });
        });
    }
}

module.exports = SqlitePromised;