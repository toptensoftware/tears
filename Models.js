"use strict"

var util = require('util');
var merge = require('merge');
var SQL = require('./SQL');

class ModelInstanceBase
{
	constructor(values)
	{
		if (values)
		{
			for (var key in values)
			{
				if (values.hasOwnProperty === undefined || values.hasOwnProperty(key))
				{
					this[key] = values[key];
				}
			}
		}
	}

	// Build an sql statement to fetch a row with supplied primary key value
	// and maps the retrieved row to a model instance
	// eg: models.User.row(23) => SELECT * FROM Users WHERE id=?  [23]
	static row(condition)
	{
		var sql = SQL.select()
				.from(this.prototype.modelOptions.tableName);

		if (util.isObject(condition))
			sql.where(condition);
		else
			sql.where(`${this.prototype.modelOptions.primaryKey}=?`, condition);

		sql.mapRow = this.mapRow.bind(this);
		return sql;
	}

	// Build an SQL statement to fetch rows
	// and maps the retrieved row to a model instance
	// eg: models.User.select(...) => SELECT ... FROM Users
	static select()
	{
		var sql = new SQL();
		sql.select.apply(sql, arguments);
		sql.from(this.prototype.modelOptions.tableName);
		sql.mapRow = this.mapRow.bind(this);
		return sql;
	}

	static mapRow(r)
	{
		if (!r)
			return null;
		var inst = new this(r);
		inst["$originalValues"] = r;
		if (inst.onLoaded)
			inst.onLoaded();
		return inst;
	}

	postInsert(r)
	{
		// Now that values have been inserted, update our original
		// values in case object is used in a subsequent update
		this["$originalValues"] = this["$insertedValues"];
		delete this["$insertedValues"];

		if (this.modelOptions.primaryKeyIsAutoIncrement)
		{
			// If table has a primary key, set the last row id on the
			// object and return the object
			this[this.modelOptions.primaryKey] = r.lastID;
			return this;
		}
		else
		{
			// Just return the row id
			return r.lastID;
		}
	}

	postUpdate()
	{
		// Now that values have been inserted, update our original
		// values in case object is used in a subsequent update
		this["$originalValues"] = this["$updatedValues"];
		delete this["$updatedValues"];
		return this;
	}
}

class Models
{
	static define(name, options)
	{
		if (typeof(options)==="string")
		{
			options = { tableName: options };
		}

		if (options.standardColumns!==false)
		{
			options = merge({
				primaryKey: "id",
				primaryKeyIsAutoIncrement: true,
				createdAtColumn: "createdAt",
				updatedAtColumn: "updatedAt",
			}, options);
		}

		// Create a dynamically named class
		var modelClass = {[name]: class extends ModelInstanceBase
		{
			constructor(values)
			{
				super(values)
			}
		}}[name];

		// Store the model options on the prototype
		modelClass.prototype.modelOptions = options;

		return modelClass;
	}

	static setupInsertDates(modelOptions, values)
	{
		var now = Date.now();

		// Setup created date
		if (modelOptions.createdAtColumn && !values[modelOptions.createdAtColumn])
		{
			values[modelOptions.createdAtColumn] = now;
		}

		// Setup updated date
		if (modelOptions.updatedAtColumn && !values[modelOptions.updatedAtColumn])
		{
			values[modelOptions.updatedAtColumn] = now;
		}
	}

	static setupUpdateDate(modelOptions, values)
	{
		var now = Date.now();

		// Setup updated date
		if (modelOptions.updatedAtColumn)
		{
			values[modelOptions.updatedAtColumn] = now;
		}
	}

	static buildInsert(type, values)
	{
		// Table name and values
		// eg: insert("Users", { firstName: "Joe", lastName: "Sixpack" });
		if (typeof(type) === "string" && util.isObject(values))
		{
			return SQL.insert(type).values(values);
		}

		// Model class and values
		// eg: insert(models.User, { firstName: "Joe", lastName: "Sixpack" });
		if (typeof(type) === "function" && type.prototype.modelOptions)
		{
			Models.setupInsertDates(type.prototype.modelOptions, values);

			return SQL.insert(type.prototype.modelOptions.tableName).values(values);
		}

		// Object instance
		// eg: insert(new models.User({ firstName: "Joe", lastName: "Sixpack" }));
		if (type instanceof ModelInstanceBase)
		{
			Models.setupInsertDates(type.modelOptions, type);

			var sql = SQL.insert(type.modelOptions.tableName).values(type);

			// Save the inserted values
			type["$insertedValues"] = sql.values;
			delete sql.values;

			// Setup post insert handler
			sql.postInsert = ModelInstanceBase.prototype.postInsert.bind(type); 
			return sql;
		}

		throw new Error("Don't know how to build insert statement from supplied args");
	}

	static buildDelete(type, condition)
	{
		// Table name and values
		// eg: delete("Users", { firstName: "Joe", lastName: "Sixpack" });
		if (typeof(type) === "string" && util.isObject(condition))
		{
			return SQL.delete(type)
						.where(condition);
		}

		// Model class and condition
		// eg: delete(model.User, ...);
		if (typeof(type) === "function" && type.prototype.modelOptions)
		{
			// eg: delete(models.User, { firstName: "Joe", lastName: "Sixpack" });
			if (util.isObject(condition))
			{
				return SQL.delete(type.prototype.modelOptions.tableName)
							.where(condition);
			}

			// eg: delete(model.User, 23);
			if (type.prototype.modelOptions.primaryKey)
			{
				return SQL.delete(type.prototype.modelOptions.tableName)
							.where(`${type.prototype.modelOptions.primaryKey} = ?`, condition);
			}
		}

		// Object instance
		// eg: delete(new model.User({ id: 23 }));
		if (type instanceof ModelInstanceBase)
		{
			if (type.modelOptions.primaryKey)
			{
				return SQL.delete(type.modelOptions.tableName)
							.where(`${type.modelOptions.primaryKey} = ?`, type[type.modelOptions.primaryKey]);
			}
		}

		throw new Error("Don't know how to build delete statement from supplied args");
	}

	static buildUpdate(type, values, condition)
	{
		// Table name and values
		// eg: update("Users", { firstName: "Joe", lastName: "Sixpack" }, { id: 23 });
		if (typeof(type) === "string" && util.isObject(values) && util.isObject(condition))
		{
			return SQL.update(type)
						.set(values)
						.where(condition);
		}

		// Model class and values
		// eg: update(models.User, ...);
		if (typeof(type) === "function" && type.prototype.modelOptions)
		{
			Models.setupUpdateDate(type.prototype.modelOptions, values);

			// eg: update(models.User, { firstName: "Joe", lastName: "Sixpack" }, { id: 23 });
			if (util.isObject(condition))
			{
				return SQL.update(type.prototype.modelOptions.tableName)
							.set(values)
							.where(condition);
			}

			// eg: update(models.User, { firstName: "Joe", lastName: "Sixpack" }, 23);
			if (type.prototype.modelOptions.primaryKey)
			{
				return SQL.update(type.prototype.modelOptions.tableName)
							.set(values)
							.where(`${type.prototype.modelOptions.primaryKey} = ?`, condition);
			}
		}

		// Object instance
		// eg: update(new models.User({ firstName: "Joe", lastName: "Sixpack" }));
		if (type instanceof ModelInstanceBase)
		{
			if (type.modelOptions.primaryKey)
			{
				Models.setupUpdateDate(type.modelOptions, type);

				var sql = SQL.update(type.modelOptions.tableName)
							.set(type, type["$originalValues"])
							.where(`${type.modelOptions.primaryKey} = ?`, type[type.modelOptions.primaryKey]);
				type["$updatedValues"] = sql.values;
				delete sql.values;

				// Post update handler
				sql.postUpdate = ModelInstanceBase.prototype.postUpdate.bind(type);
				return sql;
			}
		}

		throw new Error("Don't know how to build update statement from supplied args");
	}
}


module.exports = Models;
