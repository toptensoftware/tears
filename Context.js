"use strict"

var SqlitePromised = require('./SqlitePromised');
var SQL = require('./SQL');
var Models = require('./Models');
var ContextBase = require('./ContextBase');

/**
 * Provides a Context on which database operations can be performed
 * 
 * An instance of this class is passed to the callback invoked from Database.transaction()
 * It is also automatically instantiated and use internally when and ContextBase db operations
 * are performed directly on a Database instance. eg: db.run() internally creates a context and invokes
 * it.
 * 
 * You should never create instances of this class directly
 * 
 * @class Context
 * @extends ContextBase
 */
class Context extends ContextBase
{
	constructor(owner, readOnly)
	{
        super();
		this.owner = owner;
		this.claim = null;
		this.txDepth = 0;
		this.log = owner.log;
		this.readOnly = readOnly;
	}

	async open()
	{
		if (this.claim)
			throw new Error("Already open");

		this.claim = await this.owner.takeClaim(this.readOnly);
	}

	async close()
	{
		if (!this.claim)
			throw new Error("Not open");

		// If in transaction, automatically roll it back
		if (this.txDepth!=0)
		{
			await this.rollback();
		}

		// Release the claim
		this.owner.releaseClaim(this.claim);
		this.claim = null;
	}

    /**
     * Begin a new transaction
     * 
     * Calls can be nested and transaction will commit when
     * a balanced number of matching commits are made.
     * 
     * Calling rollback in any nesting aborts the entire transaction
	 * @method beginTransaction
     */
	async beginTransaction()
	{
		if (!this.claim)
			throw new Error("Not open");
		if (this.readOnly)
			throw new Error("Operation is not valid on readonly context");

		this.txDepth++;
		if (this.txDepth==1)
		{
			await this.run("SAVEPOINT tearstx").then(() => this);
		}
		return this;
	}

    /**
     * Commits a previous opened transaction.
     * 
     * Commit only takes place once a balanced number of beginTransaction()/commit()
     * calls have been made.
     * 
     * Commit will be ignore if rollback() is called.
	 * @method commit
     */
	async commit()
	{
		if (!this.claim)
			throw new Error("Not open");
		if (this.readOnly)
			throw new Error("Operation is not valid on readonly context");

		if (this.txDepth==0)
			return;

		this.txDepth--;

		if (this.txDepth==0)
		{
			return await this.run("RELEASE SAVEPOINT tearstx");
		}
	}

    /**
     * Rolls back a previously started transaction, aborting
     * all nested transactions.
	 * @method rollback
     */
	async rollback()
	{
		if (!this.claim)
			throw new Error("Not open");
		if (this.readOnly)
			throw new Error("Operation is not valid on readonly context");

		if (this.txDepth==0)
			return;

		this.txDepth=0;
		return await this.run("ROLLBACK TO SAVEPOINT tearstx");
	}

	async transaction(cb) 
	{
		let retv = null;

		// Start transaction
		await this.beginTransaction();

		// Invoke the transaction callback
		try
		{
			retv = await cb(this);
		}
		catch (x) 
		{
			// Rollback on exception
			await this.rollback();
			throw x;
		}

		// Commit on successfull execution
		await this.commit();

		return retv;
	}

	async run()
	{
		if (!this.claim)
			throw new Error("Not open");
		if (this.readOnly)
			throw new Error("Operation is not valid on readonly context");

		var sql = SQL.fromArgs(arguments);
		this.log(sql);
		return await this.claim.db.run(sql.sql, sql.params);
	}

	async get()
	{
		if (!this.claim)
			throw new Error("Not open");

		// eg: get(User, 23)
		// or: get(User, { first: "joe" })
		var sql;
		if (arguments.length==2)
		{
			var type = arguments[0];
			if (typeof(type) === "function" && type.prototype.modelOptions)
			{
				sql = type.row(arguments[1]);
			}
		}

		if (!sql)
			sql = SQL.fromArgs(arguments);

		// Execute SQL
		this.log(sql);
		var row = await this.claim.db.get(sql.sql, sql.params);

		// Map rows?
		if (sql.mapRow)
		{
			return sql.mapRow(row);
		}
		else
		{
			return row;
		}
	}

	async all()
	{
		if (!this.claim)
			throw new Error("Not open");

		var sql = SQL.fromArgs(arguments);

		this.log(sql);
		var rows = await this.claim.db.all(sql.sql, sql.params);

		if (sql.mapRow)
		{
			return rows.map(sql.mapRow);
		}
		else
		{
			return rows;
		}
	}

	async insert()
	{
		if (!this.claim)
			throw new Error("Not open");
		if (this.readOnly)
			throw new Error("Operation is not valid on readonly context");

		var sql = Models.buildInsert.apply(null, arguments);

		var result = await this.run(sql);

		if (sql.postInsert)
		{
			return sql.postInsert(result);
		}
		else
		{
			return result;
		}
	}

	async delete()
	{
		if (!this.claim)
			throw new Error("Not open");
		if (this.readOnly)
			throw new Error("Operation is not valid on readonly context");

		var sql = Models.buildDelete.apply(null, arguments);

		var result = await this.run(sql);

		if (sql.postDelete)
		{
			return sql.postDelete(result);
		}
		else
		{
			return result;
		}
	}

	async update()
	{
		if (!this.claim)
			throw new Error("Not open");
		if (this.readOnly)
			throw new Error("Operation is not valid on readonly context");

		var sql = Models.buildUpdate.apply(null, arguments);

		var result = await this.run(sql);

		if (sql.postUpdate)
		{
			return sql.postUpdate(result);
		}
		else
		{
			return result;
		}
	}

	async createTable(options)
	{
		await this.transaction(async () => {

			// Create the table
			await this.run(SQL.createTable(options));

			// Create the indicies
			if (options.indicies)
			{
				await Promise.all(options.indicies.map((indexDef) => {

					// Use table name if not explicitly set on index definition
					if (!indexDef.tableName)
						indexDef.tableName = options.tableName;
	
					// Run it
					return this.run(SQL.createIndex(indexDef));
				}));
			}
		});
	}

}


module.exports = Context;