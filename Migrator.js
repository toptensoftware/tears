"use strict"

var Database = require('./Database.js');
var SQL = require('./SQL.js');
var fs = require('fs');


class Migrator
{

	// action = 
	//  "up", 					- process one up migration
	//  "down",					- process one down migration
	//  "all"					- process all up migrations
	//  "redo"					- undo and then redo the last migration
	//  "reset"					- undo all migrations
	//  "rebuild"				- undo and then redo all migrations

	async run(dbfile, action)
	{
		// Create DB pool
		var db = new Database(dbfile);

		await db.transaction(async (tx) => {

			this.ctx = tx;

			// Get the list of already processed migrations
			this.processedMigrations = await this.getProcessedMigrationsList(tx);

			// Get the list of all migrations
			this.allMigrations = this.getAllMigrations();

			switch (action)
			{
				case "up": await this.process(true); break;
				case "down": await this.process(false); break;
				case "all": await this.processAll(); break;
				case "redo": await this.processRedo(); break;
				case "reset": await this.processReset(); break;
				case "rebuild": await this.processRebuild(); break;
			}

		});
	}


	// Retrieve the list of already processed migrations
	async getProcessedMigrationsList(ctx)
	{
		try
		{
			return (await ctx.all(SQL.select().from("tears"))).map(x=>x.migration);
		}
		catch (e)
		{
			if (e.message.indexOf("no such table")<0)
				throw e;

			console.log("Creating migrations table...");

			await ctx.run(SQL.createTable({
				tableName: "tears",
				standardColumns: false,
				columns:
				[
					{ migration: "STRING" },
				]
			}));

			return [];
		}
	}

	getAllMigrations()
	{
		var files = fs.readdirSync("./migrations")
			.filter(x => x.endsWith(".js"))				// Only js files
			.map(x => x.substring(x, x.length - 3))		// remove extension
			.sort();

		return files;
	}

	async process(up)
	{
		// Find current position
		var i,j;
		for (i=0, j=0; i<this.allMigrations.length; i++)
		{
			if (j<this.processedMigrations.length)
			{
				if (this.processedMigrations[j] == this.allMigrations[i])
				{
					j++;
				}
				else
				{
					console.log("Skipping retro-actively inserted migration: ", this.allMigrations[i]);
				}
				continue;
			}
			break;
		}

		if (up)
		{
			if (i<this.allMigrations.length)
			{
				var migrationName = this.allMigrations[i];

				console.log("Up: ", migrationName);

				// Load it
				var migration = require(process.cwd() + "/migrations/" + migrationName + ".js");

				// Create tables
				if (migration.tables)
				{
					for (var tableSpec of migration.tables)
					{
						await this.ctx.createTable(tableSpec);
					}
				}
				
				// Run it up
				if (migration.up)
				{
					await migration.up(this.ctx);
				}

				// Record it
				await this.ctx.insert("tears", { migration: migrationName});
				this.processedMigrations.push(migrationName);
				return true;
			}
		}
		else
		{
			if (i>0)
			{
				var migrationName = this.allMigrations[i-1];

				console.log("Down: ", migrationName);

				// Load it
				var migration = require(process.cwd()  + "/migrations/" + migrationName + ".js");
				
				// Run it
				if (migration.down)
				{
					await migration.down(this.ctx);
				}

				// Drop tables
				if (migration.tables)
				{
					for (var tableSpec of migration.tables.reverse())
					{
						await this.ctx.run(`DROP TABLE IF EXISTS \`${tableSpec.tableName}\``);
					}
				}
				


				// Record it
				await this.ctx.delete("tears", { migration: migrationName });
				this.processedMigrations.splice(i - 1);	
				return true;
			}
		}

		return false;
	}

	async processAll()
	{
		while (await this.process(true))
		{
		}
	}

	async processRedo()
	{
		if (await this.process(false))
		{
			await this.process(true);
		}
	}

	async processReset()
	{
		while (await this.process(false))
		{
		}
	}

	async processRebuild()
	{
		while (await this.process(false))
		{
		}
		while (await this.process(true))
		{
		}
	}
}

module.exports = {
	run: async function(dbfile, args)
	{
		var action = "all";
		if (!args)
			args = process.argv.slice(2);
		if (args.length>0)
			action = args[0];
		var mig = new Migrator();
		return await mig.run(dbfile, action);
	}
};