"use strict"

/**
 * Common base class to Database and Context
 * and defines the common DB access methods available on both classes
 * 
 * @class ContextBase
 * @constructor
 */
class ContextBase
{
	/**
	 * Run a query that doesn't return rows
	 * 
	 * @example
	 * 
	 *     // Plain SQL with parameters
	 *     await db.run("DELETE FROM Users WHERE id=?", 123);
	 *     
	 *     // Using SQL builds
	 *     await db.run(new SQL()
	 *     			.delete()
	 *     			.from("Users")
	 *    			.where("id=?", 123)
	 *     			);
	 * 
	 * @param {String|SQL|ModelClass} query The query to run (string or SQL Object) or model class
	 * @param {Object} [params] Optional map of parameters to the query, or primary key value
	 * @return {Promise|Object} A promise to provide result with { changes: N, lastID: M }
	 * @method run
	 */
	run() {}


	/**
	 * Run a single row query
	 * 
	 * @example
	 *     // Get model using primary key
	 *     await db.get(Models.User, 23);
	 *     
	 *     // Get model matching arbitrary fields
	 *     await db.get(Models.User , {firstName: "Joe", lastName: "Sixpack" });
	 *     
	 *     // Get model using an SQL query
	 *     await db.get(User.select().where("balance > ?", 1000));
	 *     
	 *     // Get object using SQL builder
	 *     // (the returned object will be a normal object map, not a model class)
	 *     await db.get(SQL.select().from("Users").where("balance > ?", 1000));
	 *     
	 *     // Get object using plain SQL
	 *     await db.get("SELECT * FROM Users WHERE balance > ?", 1000);
	 * 
	 * @param {String|SQL|ModelClass} query The query to run (string or SQL Object) or model class
	 * @param {Object} [params] Optional map of parameters to the query, or primary key value
	 * @return {Promise|Object} Promise to provide either an object or a model instance
	 * @method get
	 */
	get() {}

	/**
	 * Run a multi row query
	 * 
	 * @example
	 *     // Get models matching arbitrary fields
	 *     await db.get(Models.User , {firstName: "Joe", lastName: "Sixpack" });
	 *     
	 *     // Get models using an SQL query
	 *     await db.get(User.select().where("balance > ?", 1000));
	 *     
	 *     // Get objects using SQL builder
	 *     // (the returned object will be a normal object map, not a model class)
	 *     await db.get(SQL.select().from("Users").where("balance > ?", 1000));
	 *     
	 *     // Get objects using plain SQL
	 *     await db.get("SELECT * FROM Users WHERE balance > ?", 1000);
	 * 
	 * @param {String|SQL|ModelClass} query The query to run (string or SQL Object) or model class
	 * @param {Object} [params] Optional map of parameters to the query, or primary key value
	 * @return {Promise|Object[]>} Promise to provide an array of either objects or a model instances
	 * @method all
	 */
	all() {}

	/**
	 * Insert a row
	 * 
	 * @example
	 *     // Insert using table name
	 *     await db.insert("Users", { firstName: "Joe", lastName: "Sixpack" });
	 *     
	 *     // Insert using model class
	 *     await db.insert(User, { firstName: "Joe", lastName: "Sixpack" });
	 *     
	 *     // Insert model instance
	 *     var user = new models.User({ firstName: "Joe", lastName: "Sixpack" })
	 *     await insert(user);
	 *     assert(user.id);			// If auto increment primary key, will be populated here
	 * 
	 * @param {String|ModelClass|ModelInstance} table The table name, model class, or a model instance
	 * @param {Object} [params] Optional map of values to be inserted
	 * @return {Promise|Object} A promise to provide result with { changes: N, lastID: M }
	 * @method insert
	 */
	insert() {}

	/**
	 * Delete a row
	 * 
	 * @example
	 *     // Delete using table name and conditon map
	 *     await db.delete("Users", { firstName: "Joe", lastName: "Sixpack" });
	 *     
	 *     // Delete using model class and condition map
	 *     await db.delete(models.User, { firstName: "Joe", lastName: "Sixpack" });
	 *     
	 *     // Delete using model class and primary key
	 *     await db.delete(model.User, 23);
	 *     
	 *     // Delete using model instance
	 *     await db.delete(new model.User({ id: 23 }));
	 * 
	 * @param {String|ModelClass|ModelInstance} table The table name, model class, or a model instance
	 * @param {Object} [params] Optional map of conditions to match, or a primary key value
	 * @return {Promise|Object} A promise to provide result with { changes: N, lastID: M }
	 * @method delete
	 */
	delete() {}

	/**
	 * Update a row
	 * 
	 * @example
	 *     // Update using table name, new values and condition map
	 *     await db.update("Users", { firstName: "Joe", lastName: "Sixpack" }, { id: 23 });
	 *     
	 *     // Update using model class, new values and condition map
	 *     await db.update(User, { firstName: "Joe", lastName: "Sixpack" }, { id: 23 });
	 *     
	 *     // Update using model class, new values and primary value value
	 *     await db.update(User, { firstName: "Joe", lastName: "Sixpack" }, 23);
	 *     
	 *     // Update previously retrieved model instance
	 *     var user = await db.get(User, 123);
	 *     user.firstName = "Jolene";
	 *     await db.update(user));
	 * 
	 * @param {String|ModelClass|ModelInstance} table The table name, model class, or a model instance
	 * @param {Object} [params] A map of new values (unless using model instance)
	 * @param {Object} [params] An optional map of conditions to match, or a primary key value (unless using model instance)
	 * @return {Promise|Object} A promise to provide result with { changes: N, lastID: M }
	 * @method update
	 */
	update() {}


	/**
	 * Creates and table and associated indicies
	 * 
	 * @example
	 *     // Create table with indicies
	 *     await db.createTable({
	 *    		tableName: "Users",
	 *    		columns:
	 *    		[
	 *    			{ name: "STRING" },
	 *    			{ email: "STRING" },
	 *    		],
	 *    		indicies:
	 *    		[
	 *    			{
	 *    				unique:true,
	 *    				columns:
	 *    				[
	 *    					{ email: "ASC" },
	 *    				]
	 *    			}
	 *    		]
	 *     });
	 * 
	 * @param {Object} options 
     * @method createTable
	 */
	createTable(options) {}

    /**
     * Begins a transaction, calls a callback function to perform
     * the database operations and then automatically commits the
     * transaction.
     * 
     * If an exception is thrown during the callback, the transaction is rolled back.
     * 
     * IMPORTANT: All transaction operations must be executed on the
     * Context object passed to the callback function.
     * 
     * @example
     *     await db.transaction((ctx) => {
     *     
     *          // NB: Use `ctx`, not `db`
     *          await ctx.run("DELETE FROM Users");
     *          await ctx.run("DELETE FROM Sessions");
     *     
     *     });
     * @param {Function} cb The callback to perform the transaction operations
     * @method transaction
     */
    transaction() {}
}


module.exports = ContextBase;