/**
 * Tears
 * 
 * @class tears
 */


module.exports = {

    /**
     * Short cut to the Database class 
     * @example
     * 
     *     var tears = require('tears');
     *     var db = new tears.Database('db.sqlite');
     * 
     * @property {Database} Database
     */
    Database: require('./Database'),


    /**
     * Short cut to the SQL class 
     * @example
     * 
     *     var tears = require('tears');
     *     var sql = tears.SQL.select("*").from("Users")...
     * 
     * @property {SQL} SQL
     */
    SQL: require('./SQL'),

    /**
     * Runs the migrator tool
     * 
     * The migrator looks for files of the format "NNNN-*" in a sub-directory of the 
     * current directory named 'migrations'.
     * 
     * It's probably worth creating a migrate.js file similar to the following your project directory
     * 
     *     require('tears').migrate("db.sqlite");
     * 
     * The you can run the migrator by:
     * 
     *     > node migrate
     * 
     * Or with a command like so...
     * 
     *     > node migrate down
     * 
     * @example
     * 
     *     // Example Migrations Script
     *     // "migrations/0001-base.js"
     *     module.exports = {
     *   
     *         // Tables and indices will be created on "up", dropped on "down"
     *         tables: 
     *         [
     *             {
     *                 tableName: "Users",
     *                 columns:
     *                 [
     *                     { name: "STRING" },
     *                     { email: "STRING" },
     *                 ],
     *                 indicies:
     *                 [
     *                     {
     *                     unique:true,
     *                     columns:
     *                         [
     *                             { email: "ASC" },
     *                         ]
     *                     }
     *                 ]
     *             }
     *         ],
     *         
     *         up: async function(db)
     *         {
     *             await db.run("ALTER TABLE Templates ADD COLUMN markdown STRING;");
     *         },
     *         
     *         down: async function(db)
     *         {
     *             // Unfortunately no drop column in sqlite :(
     *         }
     *     };
     * 
     * @method migrate
     * @param {String} fileName Path to the database file to migrate
     * @param {String} [command] Command to execute "all", "up", "down", "redo", "reset", "rebuild"
     *                           If parameter no supplied, will read it from process.argv or default to "all".
     */
    migrate: require('./Migrator').run,

    /**
     * Defines a model class
     * @example
     *     var tears = require('tears');
     * 
     *     var User = tears.definedModel("User", "Users");
     *     var TagMap = tears.defineModel("TagMap", { tableName: "TagMap", standardColumns: false });
     * 
     * @param {String} className The name of the class to define
     * @param {String|Object} options The name of the table the class maps to, or an object map of options
     * @param {String} options.tableName Name of the table associated with this model
     * @param {Boolean} options.standardColumns Set to false to prevent creation of standard columns
     * @param {String} options.primaryKey The primary key (defaults to "id" unless standardColumns is false)
     * @param {Boolean} options.primaryKeyIsAutoIncrement If the primary key is auto increment will be automatically populated after insert (defaults to true unless standardColumns is false)
     * @param {String} options.createdAtColumn Name of the "created at" column (defaults to "createdAt" unless standardColumns is false)
     * @param {String} options.updatedAtColumn Name of the "updated at" column (defaults to "updatedAt" unless standardColumns is false)
     * @method defineModel
     */
    defineModel: require('./Models').define,
};