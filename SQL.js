"use strict"

/**
 * A simple fluent style query builder that builds an SQL statement and a set of associated parameters
 * 
 * @class SQL
 * @constructor
 * @param {String|SQL} [sql] The text or another SQL instance to append
 * @param {Object[]} [params] The parameters to record with the query
 * 
 * @example
 * 
 *     // Simple query and param
 *     var sql = new SQL("SELECT * FROM Users WHERE id=?", 123)
 *     
 *     // Join multiple parts together
 *     var sql = new SQL("SELECT *" FROM USERS")
 *     			.append("WHERE balance > ?", 1000)
 *     			.append("  AND position = ?", "manager")
 *     
 *     // Handy methods for building a query
 *     var sql = new SQL().select().from("Users")
 *     			.where("balance > ?", 1000)
 *     			.and("position = ?", "manager");
 * 
 */
class SQL
{
	constructor()
	{
		this.sql = "";
		this.params = [];
		this.append.apply(this, arguments)
	}	

	static fromArgs(args)
	{
		if (args.length==1 && args[0] instanceof SQL)
			return args[0];

		var sql = new SQL();
		sql.append.apply(sql, args);
		return sql;
	}

	/**
	 * Append text and parameters to this query
	 * 
	 * @example
	 * 
	 * 		// Simple text and parameter
	 * 		SQL.append("WHERE x=?", 10);
	 * 
	 * 		// Multiple parameters
	 * 		SQL.append("WHERE x=? AND y=?", 10, 20);
	 * 
	 * 		// Multiple parameters as an array
	 * 		SQL.append("WHERE x=? AND y=?", [ 10, 20 ]);
	 * 
	 * 		// Append another SQL instance
	 * 		var baseQuery = new SQL("SELECT * FROM Users");
	 * 		var condition = new SQL("WHERE Age > ?", 21);
	 * 		baseQuery.append(condition);
	 * 
	 * @param {String|SQL} [sql] The text or another SQL instance to append
	 * @param {Object[]} [params] The parameters to record with the query
	 * @return	{SQL} A reference to this object
	 * @method append
	 */
	append(sql, params)
	{
		delete this.hasWhere;

		if (sql===undefined || sql===null)
			return;

		// Append another sql builder?
		if (sql instanceof SQL)
		{
			return this.append(sql.sql, sql.params);
		}

		// First string?
		if (this.sql.length>1)
			this.sql += " " + sql;
		else
			this.sql = sql;

		if (params!==undefined)
		{
			// If params is an array append it, else append all other params
			if (params instanceof Array)
			{
				this.params = this.params.concat(params);
			}
			else if (arguments.length>1)
			{
				this.params = this.params.concat(Array.prototype.slice.call(arguments, 1));
			}
		}

		return this;
	}

	/**
	 * Append a "SELECT" statement
	 * 
	 * @example
	 * 		// "SELECT *"
	 * 		sql.select();
	 * 
	 * 		// "SELECT COUNT(*)"
	 * 		sql.select("COUNT(*)");
	 * 
	 * 		// "SELECT" columns
	 * 		sql.select("firstName, lastName");
	 * 
	 * @param {String|SQL} [sql] Text or another SQL instance to append
	 * @param {Object[]} [params] Parameters to record with the query
	 * @return	{SQL} A reference to this object
	 * @method select
	 */
	select()
	{
		this.append("SELECT");
		if (arguments.length==0)
			return this.append("*");
		else
			return this.append.apply(this, arguments);
	}

	/**
	 * Append a "INSERT INTO" statement
	 * 
	 * @example
	 * 		// "INSERT INTO Users"
	 * 		sql.insert("Users");
	 * 
	 * @param {String|SQL} [sql] Optionsal text or another SQL instance to append
	 * @param {Object[]} [params] Optionals parameters to record with the query
	 * @return	{SQL} A reference to this object
	 * @method insert
	 */
	insert()
	{
		this.append("INSERT INTO");
		return this.append.apply(this, arguments)
	}

	/**
	 * Append values to an INSERT statement
	 * 
	 * @example
	 * 		// Insert 
	 * 		new SQL().insert("Users").values({
	 * 			firstName: "Joe",
	 * 			lastName: "Sixpack",
	 * 		});
	 * 
	 * @param {Object} [values] A map of column name to value to insert
	 * @return	{SQL} A reference to this object
	 * @method values
	 */
	values(values)
	{
		var columnNames = [];
		var params = [];
		var paramValues = [];
		this.values = { };
		for (var key in values)
		{
			if (key[0]=='$' || key[0]=='.')
				continue;
			if ((values.hasOwnProperty === undefined || values.hasOwnProperty(key)) && !(values[key] instanceof Function) && values[key]!==undefined) 
			{
				this.values[key] = values[key];
				columnNames.push('`' + key + '`');
				params.push('?');
				paramValues.push(values[key]);
			}
		}

		return this.append(`(${columnNames.join()}) VALUES (${params.join()})`, paramValues)
	}

	/**
	 * Append a "DELETE FROM" statement
	 * 
	 * @example
	 * 		// "DELETE FROM Users"
	 * 		sql.delete("Users");
	 * 
	 * @param {String|SQL} [sql] Optionsal text or another SQL instance to append
	 * @param {Object[]} [params] Optionals parameters to record with the query
	 * @return	{SQL} A reference to this object
	 * @method delete
	 */
	delete()
	{
		this.append("DELETE FROM");
		return this.append.apply(this, arguments)
	}

	/**
	 * Append an "UPDATE" statement
	 * 
	 * @example
	 * 		// "UPDATE Users"
	 * 		sql.update("Users");
	 * 
	 * @param {String|SQL} [sql] Optionsal text or another SQL instance to append
	 * @param {Object[]} [params] Optionals parameters to record with the query
	 * @return	{SQL} A reference to this object
	 * @method update
	 */
	update()
	{
		this.append("UPDATE");
		return this.append.apply(this, arguments)
	}

	/**
	 * Append a "SET" statement to an UPDATE statement
	 * 
	 * @example
	 * 		// "SET firstName=?, lastName=?", "Joe", "SixPack"
	 * 		sql.update("Users").set({
	 * 			firstName: "Joe",
	 * 			lastName: "SixPack"
	 * 		});
	 * 
	 * @param {String|Object} [values] Either direct string, or object of values
	 * @param {Object} [originalValues] Optional object of original values, only changed values will be included
	 * @return	{SQL} A reference to this object
	 * @method set
	 */
	set(values, originalValues)
	{
		this.append("SET");

		if (typeof(values)==="string")
			return this.append.apply(this, arguments)

		var setExpressions = [];
		var paramValues = [];
		this.values = {};
		for (var key in values)
		{
			if (key[0]=='$' || key[0]=='.')
				continue;
			if ((values.hasOwnProperty === undefined || values.hasOwnProperty(key)) && !(values[key] instanceof Function)) 
			{
				this.values[key] = values[key];

				// Ignore unchanged fields
				if (originalValues && originalValues[key] && originalValues[key] == values[key])
					continue;

				setExpressions.push("`" + key + "` = ?");
				paramValues.push(values[key]);
			}
		}

		return this.append(setExpressions.join(), paramValues);
	}

	/**
	 * Append a "FROM" statement
	 * 
	 * @example
	 * 		// "FROM Users"
	 * 		sql.delete("Users");
	 * 
	 * @param {String|SQL} [sql] Optional text or another SQL instance to append
	 * @param {Object[]} [params] Optional parameters to record with the query
	 * @return	{SQL} A reference to this object
	 * @method from
	 */
	from()
	{
		this.append("FROM");
		return this.append.apply(this, arguments)
	}

	/**
	 * Append a "WHERE" statement
	 * 
	 * @example
	 * 
	 * 		// "WHERE x=?", 99
	 * 		sql.where({x: 99});
	 * 
	 * 		// "WHERE x=? AND y=?", 10, 20
	 * 		sql.where({x:10, y:20});
	 * 
	 * @param {Object} [condition] 
	 * @return	{SQL} A reference to this object
	 * @method where
	 */
	where()
	{
		this.append("WHERE");
		this.append(SQL.buildCondition.apply(undefined, arguments));
		this.hasWhere = true;
		return this;
	}

	/**
	 * Append a "WHERE" or "AND" statement.  
	 * 
	 * If the previous statement was a "WHERE" then appends "AND"
	 * Otherwise appends a "WHERE" 
	 * 
	 * @param {Object} [condition] 
	 * @return	{SQL} A reference to this object
	 * @method andWhere
	 */
	andWhere()
	{
		if (this.hasWhere)
			this.and.apply(this, arguments);
		else
			this.where.apply(this, arguments);
		this.hasWhere = true;
		return this;
	}

	/**
	 * Append a "WHERE" or "OR" statement.  
	 * 
	 * If the previous statement was a "WHERE" then appends "OR"
	 * Otherwise appends an "WHERE" statement
	 * 
	 * @param {Object} [condition] 
	 * @return	{SQL} A reference to this object
	 * @method orWhere
	 */
	orWhere()
	{
		if (this.hasWhere)
			this.or.apply(this, arguments);
		else
			this.where.apply(this, arguments);
		this.hasWhere = true;
		return this;
	}

	/**
	 * Append a "ORDER BY" statement
	 * 
	 * @example
	 * 		// "ORDER BY price ASC"
	 * 		sql.orderBy("price ASC");
	 * 
	 * @param {String|SQL} [sql] Optional text or another SQL instance to append
	 * @param {Object[]} [params] Optional parameters to record with the query
	 * @return	{SQL} A reference to this object
	 * @method orderBy
	 */
	orderBy()
	{
		this.append("ORDER BY");
		return this.append.apply(this, arguments)
	}

	/**
	 * Append a "GROUP BY" statement
	 * 
	 * @example
	 * 		// "GROUP BY category"
	 * 		sql.orderBy("category");
	 * 
	 * @param {String|SQL} [sql] Optional text or another SQL instance to append
	 * @param {Object[]} [params] Optional parameters to record with the query
	 * @return	{SQL} A reference to this object
	 * @method groupBy
	 */
	groupBy()
	{
		this.append("GROUP BY");
		return this.append.apply(this, arguments)
	}

	/**
	 * Append a "LEFT JOIN" statement
	 * 
	 * @example
	 * 		// "LEFT JOIN tags"
	 * 		sql.leftJoin("Orders")
	 * 			.on("Users.id = Orders.userId")
	 * 
	 * @param {String|SQL} [sql] Optional text or another SQL instance to append
	 * @param {Object[]} [params] Optional parameters to record with the query
	 * @return	{SQL} A reference to this object
	 * @method leftJoin
	 */
	leftJoin()
	{
		this.append("LEFT JOIN");
		return this.append.apply(this, arguments)
	}

	/**
	 * Append a "ON" statement
	 * 
	 * @example
	 * 		sql.leftJoin("Orders")
	 * 			.on("Users.id = Orders.userId")
	 * 
	 * @param {String|SQL} [sql] Optional text or another SQL instance to append
	 * @param {Object[]} [params] Optional parameters to record with the query
	 * @return	{SQL} A reference to this object
	 * @method on
	 */
	on()
	{
		this.append("ON");
		return this.append(SQL.buildCondition.apply(undefined, arguments));
	}

	/**
	 * Append a "IN" statement
	 * 
	 * @example
	 * 		sql.where("id").in([10, 20, 30]);
	 * 
	 * @param {String|SQL} [sql] Optional text or another SQL instance to append
	 * @param {Object[]} [params] Optional parameters to record with the query
	 * @return	{SQL} A reference to this object
	 * @method in
	 */
	in()
	{
		this.append("IN");
		return this.append.apply(this, arguments)
	}

	/**
	 * Append a "AND" statement
	 * 
	 * @example
	 * 		// SQL
	 * 		sql.where("x=?", 10)
	 * 			.and("y=?", 20);
	 * 
	 * 		// Using an object map
	 * 		sql.where("x=?", 10)
	 * 			.and({ y: 10, z: 20, });
	 * 
	 * 
	 * @param {String|SQL|Object} [sql] Text, condition map or another SQL instance
	 * @param {Object[]} [params] Optional parameters to record with the query
	 * @return	{SQL} A reference to this object
	 * @method and
	 */
	and()
	{
		this.append("AND");
		return this.append(SQL.buildCondition.apply(undefined, arguments));
	}

	/**
	 * Append a "OR" statement
	 * 
	 * @example
	 * 		// SQL
	 * 		sql.where("x=?", 10)
	 * 			.or("y=?", 20);
	 * 
	 * 		// Using an object map
	 * 		sql.where("x=?", 10)
	 * 			.or({ y: 10, z: 20, });
	 * 
	 * @param {String|SQL|Object} [sql] Text, condition map or another SQL instance
	 * @param {Object[]} [params] Optional parameters to record with the query
	 * @return	{SQL} A reference to this object
	 * @method or
	 */
	or()
	{
		this.append("OR");
		return this.append(SQL.buildCondition.apply(undefined, arguments));
	}

	/**
	 * Append a "LIMIT" statement to take a particular page of records
	 * 
	 * @example
	 * 		// Take page 5 of 20 rows per page
	 * 		sql.takePage(5, 20);
	 * 
	 * @param {Number} page The zero based page number to take
	 * @param {Number} rowsPerPage The number of rows per page
	 * @return	{SQL} A reference to this object
	 * @method takePage
	 */
	takePage(page, rowsPerPage)
	{
		return this.append("LIMIT ?, ?", [(page - 1) * rowsPerPage, rowsPerPage]);
	}

	/**
	 * Append a "LIMIT" statement to limit query to a number of rows
	 * 
	 * @example
	 * 		// Take just first 10 rows
	 * 		sql.limit(10);
	 * 
	 * @param {Number} rows The number of rows to take
	 * @return	{SQL} A reference to this object
	 * @method limit
	 */
	limit(rows)
	{
		return this.append("LIMIT ?", rows);
	}

	/**
	 * Append a parenthesized sub query (ie: surrounded by '(' and ')')
	 * 
	 * Generally not required but can be handy to append a bunch of sub-queries
	 * ensuring that correct order operation is used.
	 * 
	 * @param {String|SQL|Object} [sql] Text, condition map or another SQL instance
	 * @param {Object[]} [params] Optional parameters to record with the query
	 * @return	{SQL} A reference to this object
	 * @method parens
	 */
	parens()
	{
		this.append("(");
		this.append.apply(this, arguments);
		this.append(")");
		return this;
	}

	/**
	 * Generates a CREATE TABLE statement.
	 * 
	 * @example
	 * 
	 * 		// Create a table with specified columns plus the 
	 * 		// standard columns `id`, `createdAt` and `updatedAt` columns
	 * 		SQL.createTable({
	 * 			columns: {
	 * 				firstName: "STRING",
	 * 				age: "INTEGER NOT NULL",
	 * 			}
	 * 		});
	 * 
	 * 		// Create a table without standard columns
	 * 		SQL.createTable({
	 * 			standardColumns: false,
	 * 			columns: {
	 * 				leftKey: "INTEGER NOT NULL",
	 * 				rightKey: "INTEGER NOT NULL",
	 * 			}
	 * 		});
	 * 
	 * @param {Object} options 
	 * @static
	 * @method createTable
	 */
	static createTable(options)
	{
		if (options.standardColumns!==false)
		{
			let standardColumns = [];
			
			if (!options.columns.some(x => namedValue(x).value.match(/PRIMARY KEY/i)))
			{
				standardColumns = [
					{ id: "INTEGER PRIMARY KEY AUTOINCREMENT" }
				];
			};

			// Date columns
			standardColumns = standardColumns.concat([
				{ createdAt: "DATE NOT NULL" },
				{ updatedAt: "DATE NOT NULL" }
			]);

			options.columns = standardColumns.concat(options.columns);
		}

		var columnDefs = [];
		for (var i=0; i<options.columns.length; i++)
		{
			var nv = namedValue(options.columns[i]);
			columnDefs.push("`" + nv.name + "` " + nv.value);
		}

		return new SQL().append(`CREATE TABLE \`${options.tableName}\` ( ${columnDefs.join()} );`);
	}

	/**
	 * Generates a DROP TABLE statement.
	 * 
	 * @example
	 * 
	 * 		// Create a table with specified columns plus the 
	 * 		// standard columns `id`, `createdAt` and `updatedAt` columns
	 * 		SQL.dropTable("Users");
	 * 
	 * @param {String} name The name of the table to create 
	 * @static
	 * @method dropTable
	 */
	static dropTable(name)
	{
		return new SQL().append(`DROP TABLE \`${name}\``);
	}

	/**
	 * Generates a CREATE INDEX statement.
	 * 
	 * @example
	 * 
	 * 		SQL.createIndex({
	 * 			tableName: "Users",
	 * 			unique: true,								// Optional, default = false
	 * 			indexName: "Users_firstName_lastName",		// Optional, default synthesized from table and column names
	 * 			columns: {
	 * 				lastName: "ASC",
	 * 				firstName: "DESC",
	 * 			},
	 * 		});
	 * 
	 * @param {String} name The name of the table to create 
	 * @static
	 * @method createIndex
	 */
	static createIndex(options)
	{
		var columnDefs = [];
		var columnNames = [];
		for (var i=0; i<options.columns.length; i++)
		{
			var nv = namedValue(options.columns[i]);
			columnDefs.push("`" + nv.name + "` " + nv.value);
			columnNames.push(nv.name);
		}

		var unique = options.unique ? "UNIQUE " : "";
		var indexName = options.indexName;
		if (!indexName)
			indexName = options.tableName + "_" + columnNames.join("_");

		return new SQL().append(`CREATE ${unique}INDEX ${indexName} ON ${options.tableName} ( ${columnDefs.join()} );`);
	}


	/**
	 * Creates a new SQL instance starting with a select statement
	 * @example
	 * 
	 * 		var sql = SQL.select("firstName, lastName").from("Users");
	 * 
	 * @param {String|SQL} [sql] Optionsal text or another SQL instance to append
	 * @param {Object[]} [params] Optionals parameters to record with the query
	 * @return	{SQL} A reference to this object
	 * @method select
	 * @static
	 */
	static select()
	{
		var sql = new SQL();
		return sql.select.apply(sql, arguments);
	}

	/**
	 * Creates a new SQL instance starting with a delete statement
	 * @example
	 * 
	 * 		var sql = SQL.delete().from(...
	 * 
	 * @param {String|SQL} [sql] Optionsal text or another SQL instance to append
	 * @param {Object[]} [params] Optionals parameters to record with the query
	 * @return	{SQL} A reference to this object
	 * @method delete
	 * @static
	 */
	static delete()
	{
		var sql = new SQL();
		return sql.delete.apply(sql, arguments);
	}

	/**
	 * Creates a new SQL instance starting with a update statement
	 * @example
	 * 
	 * 		var sql = SQL.update("Users").set(...
	 * 
	 * @param {String|SQL} [sql] Optionsal text or another SQL instance to append
	 * @param {Object[]} [params] Optionals parameters to record with the query
	 * @return	{SQL} A reference to this object
	 * @method update
	 * @static
	 */
	static update()
	{
		var sql = new SQL();
		return sql.update.apply(sql, arguments);
	}

	/**
	 * Creates a new SQL instance starting with a insert statement
	 * @example
	 * 
	 * 		var sql = SQL.insert("Users").values(...
	 * 
	 * @param {String|SQL} [sql] Optionsal text or another SQL instance to append
	 * @param {Object[]} [params] Optionals parameters to record with the query
	 * @return	{SQL} A reference to this object
	 * @method insert
	 * @static
	 */
	static insert()
	{
		var sql = new SQL();
		return sql.insert.apply(sql, arguments);
	}

	static buildCondition(condition)
	{
		// Alredy SQL?
		if (condition instanceof SQL)
			return condition;

		// Plain text
		if (typeof(condition) === "string")
		{
			var sql = new SQL();
			return sql.append.apply(sql, arguments);
		}

		// eg: { firstName: "Joe", lastName: "Sixpack" }
		var sql = [];
		var params = [];
		for (var key in condition)
		{
			if (key[0]=='$' || key[0]=='.')
				continue;
			if ((condition.hasOwnProperty === undefined || condition.hasOwnProperty(key)) && !(condition[key] instanceof Function)) 
			{
				sql.push("`" + key + "` = ?");
				params.push(condition[key]);
			}
		}

		return new SQL("(" + sql.join(" AND ") + ")", params);
	}

	log()
	{
		console.log(this);
		return this;
	}
}


// Picks a single named value from an object
// eg: 
//     { "myname": "somevalue" } 
// returns 
//     { name: "myname", value:"somevalue" }
function namedValue(dict)
{
	for (var key in dict)
	{
		if (key[0]=='$' || key[0]=='.')
			continue;
		if ((dict.hasOwnProperty === undefined || dict.hasOwnProperty(key)) && !(dict[key] instanceof Function)) 
		{
			return {
				name: key,
				value: dict[key]
			}
		}
	}
}

module.exports = SQL;