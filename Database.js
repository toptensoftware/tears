"use strict"

var SqlitePromised = require('./SqlitePromised');
var SQL = require('./SQL');
var Models = require('./Models');
var ContextBase = require('./ContextBase');
var Context = require('./Context');

/**
 * Represents a connection to a database.
 * 
 * The Database class allows multiple concurrent read operations but
 * only a single write operation at any one time (generally matching
 * SQLite's support for concurrency).
 * 
 * Connection pooling is not required and shouldn't be used (you'll get concurrency 
 * lock issues).
 * 
 * Generally Database instances shouldn't be created and discarded - create one
 * instance at start up (for each database) and just keep using it.
 * 
 * @example
 * // Open database and insert a record
 * var db = new Database("test.sqlite");
 * await db.insert("Users", {}
 * 		firstName: "Joe",
 * 		lastName: "SixPack",
 * )
 * 
 * @class Database
 * @extends ContextBase
 * @param {String} filename Name of the file to open
 * @param {Number} [mode=OPEN_CREATE|OPEN_READWRITE] Open mode
 * @constructor
 */
class Database extends ContextBase
{
	constructor(filename, mode)
	{
		super();
		this.db = null;
		this.dbOpen = false;
		this.filename = filename;
		this.mode = mode;
		this.activeClaims = [];
		this.pending = [];
		this.currentClaim = null;
		this.log = () => {};
	}

	async takeClaim(readOnly)
	{
		// Open the underlying database
		if (this.db == null)
		{
			this.db = new SqlitePromised();
			this.db.open(this.filename, this.mode).then(() => {
				this.dbOpen = true;
				this.resolvePendingClaims();
			}, (reason) => {
				this.rejectPendingClaims(reason);
			});
		}

		return await this.internalTakeClaim(readOnly);
	}

	releaseClaim(claim)
	{
		// Find the claim
		var pos = this.activeClaims.indexOf(claim);
		if (pos < 0)
			throw new Error("Attempt to release non-current claim");

		// Remove from active collection
		this.activeClaims.splice(pos, 1);

		// Resolve any pending claims
		this.resolvePendingClaims();
	}

	resolvePendingClaims()
	{
		// Any pending claims?
		if (this.pending.length > 0 && this.activeClaims.length == 0)
		{
			process.nextTick(() => {
				
				// Should never happen, but just in case
				if (this.pending.length == 0 || this.activeClaims.length > 0)
					return;

				// Release readonly or write claims?
				if (this.pending[0].readOnly)
				{
					// Resolve all pending readonly claims
					while (this.pending.length>0 && this.pending[0].readOnly)
					{
						var c = this.pending.shift();
						this.internalResolveClaim(c.resolve, c.readOnly)
					}
				}
				else
				{
					// Resolve just one write claim
					var c = this.pending.shift();
					this.internalResolveClaim(c.resolve, c.readOnly)
				}

			});
		}
	}

	rejectPendingClaims(reason)
	{
		while (this.pending.length > 0)
		{
			this.pending.shift().reject(reason);
		}
	}

	internalTakeClaim(readOnly)
	{
		return new Promise((resolve, reject) => {

			// If there are other pending claims, then always enqueue this one too
			if (this.pending.length == 0 && this.dbOpen)
			{
				// If there are no currently active claims then claim immediate
				if (this.activeClaims.length == 0)
					return this.internalResolveClaim(resolve, readOnly);

				// If this is a readonly claim and the current active claims
				// are all readonly then we can also resolve this one immediately
				if (readOnly && this.activeClaims[0].readOnly)
					return this.internalResolveClaim(resolve, readOnly);
			}

			// Add to the list of pending claims
			this.pending.push({
				resolve: resolve,
				reject: reject,
				readOnly: readOnly,
			});

		});
	}

	internalResolveClaim(resolve, readOnly)
	{
		// Create the claim
		var claim = {
			db: this.db,
			readOnly: readOnly,
		}

		// Add to active claim collection
		this.activeClaims.push(claim);

		// Resolve it
		resolve(claim);	
	}

	// Overrides for ContextBase
	run() { return this.invoke(false, Context.prototype.run, arguments); }
	get() { return this.invoke(true, Context.prototype.get, arguments); }
	all() { return this.invoke(true, Context.prototype.all, arguments); }
	insert() { return this.invoke(false, Context.prototype.insert, arguments); }
	update() { return this.invoke(false, Context.prototype.update, arguments); }
	delete() { return this.invoke(false, Context.prototype.delete, arguments); }
	transaction() { return this.invoke(false, Context.prototype.transaction, arguments); }
	createTable() { return this.invoke(false, Context.prototype.createTable, arguments); }

	async invoke(readOnly, fn, args)
	{
		var ctx = new Context(this, readOnly);
		await ctx.open();

		try
		{
			return await fn.apply(ctx, args);
		}
		finally
		{
			await ctx.close();
		}
	}
}

Database.defineModel = Models.define;

module.exports = Database;